package com.luis.santiago.parser.analyzer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.ReportCategory;
import com.luis.santiago.parser.report.ReportEntry;

public abstract class Strategy implements IStrategy {

	public List<String> doRegexp(String page, String strPattern) {
		Pattern pattern = Pattern.compile(strPattern);
		Matcher matcher = pattern.matcher(page);
		List<String> listMatches = new ArrayList<String>();
		while (matcher.find()) {
			processListItem(matcher, listMatches);
		}
		return listMatches;
	}

	public ReportCategory createCategory(String name, List<String> listMatches) {
		ReportCategory category = new ReportCategory(name);
		ReportCategory subcategory;
		for (int i = 0; i < listMatches.size(); i += 2) {
			subcategory = new ReportCategory("item");
			subcategory.addItem(new ReportEntry("key", listMatches.get(i)));
			subcategory.addItem(new ReportEntry("value", listMatches.get(i + 1)));
			category.addItem(subcategory);
		}
		return category;
	}

}
