package com.luis.santiago.parser.analyzer;

import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;

public class ByJob extends Strategy implements IStrategy {

	public Exportable checkSection(Element page) {
		String pattern = "([A-Za-zá-ú\\s])+con\\s([0-9.]+)";
		StringBuilder pageStr = new StringBuilder(page.toString());
		String jobsSubstring = pageStr
				.substring(pageStr.indexOf("es el formado por los") + ("es el formado por los").length());
		List<String> listMatches = doRegexp(jobsSubstring, pattern);
		return createCategory("Job", listMatches);
	}

	public Element findSection(Document page) {
		return page.select("#main").first().child(0).child(21).child(0);
	}

	public void processListItem(Matcher matcher, List<String> listMatches) {
		String[] splitted = matcher.group().split(" con ");
		listMatches.add(splitted[0].trim());
		listMatches.add(splitted[1].trim());
	}

}
