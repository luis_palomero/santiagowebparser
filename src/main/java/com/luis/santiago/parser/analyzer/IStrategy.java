package com.luis.santiago.parser.analyzer;

import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;

public interface IStrategy {
	public Exportable checkSection(Element page);

	public Element findSection(Document page);

	public void processListItem(Matcher matcher, List<String> listMatches);
}
