package com.luis.santiago.parser.analyzer;

import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.w3c.dom.Node;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class Analyzer {

	Document page;
	ArrayList<IStrategy> sectionsToCheck;

	public Analyzer() {
		sectionsToCheck = new ArrayList<IStrategy>();
	}

	public Analyzer setPage(Document page) {
		this.page = page;
		return this;
	}

	public Analyzer addStrategy(IStrategy strategy) {
		sectionsToCheck.add(strategy);
		return this;
	}

	public org.w3c.dom.Document analyze(String reportName, int month, int year) throws Exception {
		org.w3c.dom.Document xml;

		xml = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		org.w3c.dom.Element rootNode = xml.createElement("Report");
		xml.appendChild(rootNode);

		org.w3c.dom.Element periodNode = xml.createElement("Period");

		org.w3c.dom.Element monthNode = xml.createElement("Month");
		monthNode.appendChild(xml.createTextNode(Integer.toString(month)));
		periodNode.appendChild(monthNode);

		org.w3c.dom.Element yearNode = xml.createElement("Year");
		yearNode.appendChild(xml.createTextNode(Integer.toString(year)));
		periodNode.appendChild(yearNode);

		rootNode.setAttribute("name", reportName);
		rootNode.appendChild(periodNode);

		org.w3c.dom.Element categories = xml.createElement("Categories");
		rootNode.appendChild(categories);

		for (IStrategy sectionToCheck : sectionsToCheck) {
			Element elementToParse = sectionToCheck.findSection(this.page);
			Node xmlNode = sectionToCheck.checkSection(elementToParse).toXml(xml);
			categories.appendChild(xmlNode);
		}
		return xml;
	}
}
