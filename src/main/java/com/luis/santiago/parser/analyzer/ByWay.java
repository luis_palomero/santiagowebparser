package com.luis.santiago.parser.analyzer;

import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;

public class ByWay extends Strategy implements IStrategy {

	public Exportable checkSection(Element page) {
		String pattern = "([A-Za-zá-ú\\-\\s.]+) con ([0-9.]+)";
		String pageStr = new String(page.toString());
		pageStr = pageStr
				.substring(pageStr.indexOf("mayoría de los peregrinos son") + "mayoría de los peregrinos son".length());
		List<String> listMatches = doRegexp(pageStr, pattern);
		return createCategory("Way", listMatches);
	}

	public Element findSection(Document page) {
		return page.select("#main").first().child(0).child(21).child(2);

	}

	public void processListItem(Matcher matcher, List<String> listMatches) {
		String[] splittedLine = matcher.group().split(" con ");
		listMatches.add(splittedLine[0].trim());
		listMatches.add(splittedLine[1].replace(".", "").trim());
	}

}
