package com.luis.santiago.parser.analyzer;

import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;
import com.luis.santiago.parser.report.ReportCategory;
import com.luis.santiago.parser.report.ReportEntry;

public class ByGender extends Strategy implements IStrategy {

	public Exportable checkSection(Element page) {

		String pattern = "\\s([0-9.]+)\\s";
		StringBuilder pageStr = new StringBuilder(page.toString());
		String genderSubstr = pageStr.substring(pageStr.indexOf("De estos peregrinos"), pageStr.indexOf("A pie"));
		List<String> listMatches = doRegexp(genderSubstr, pattern);
		listMatches.add(0, "Mujeres");
		listMatches.add(2, "Hombres");

		return createCategory("Gender", listMatches);
	}

	public Element findSection(Document page) {
		return page.select("#main").first().child(0).child(7);
	}

	public void processListItem(Matcher matcher, List<String> listMatches) {
		listMatches.add(matcher.group(1).replace(".", "").trim());
	}

}
