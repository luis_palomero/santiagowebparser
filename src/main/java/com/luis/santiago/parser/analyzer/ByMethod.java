package com.luis.santiago.parser.analyzer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;

public class ByMethod extends Strategy implements IStrategy {

	public Exportable checkSection(Element page) {
		String pageStr = new String(page.toString());
		pageStr = pageStr.substring(pageStr.indexOf("A pie"));
		List<String> listMatches = new ArrayList<String>();

		String[] splittedPage = pageStr.split(",");
		listMatches.add("A pie");
		listMatches.add(splittedPage[0].split(" ")[4].replace(".", ""));
		listMatches.add("Bicicleta");
		listMatches.add(splittedPage[2].split(" ")[3].replace(".", ""));
		listMatches.add("Caballo");
		listMatches.add(splittedPage[4].split(" ")[3].replace(".", ""));
		listMatches.add("Silla de ruedas");
		listMatches.add(splittedPage[5].split(" ")[2].replace(".", ""));

		return createCategory("Method", listMatches);
	}

	public Element findSection(Document page) {
		return page.select("#main").first().child(0).child(7);

	}

	public void processListItem(Matcher matcher, List<String> listMatches) {
		// TODO Auto-generated method stub

	}

}
