package com.luis.santiago.parser.analyzer;

import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;

public class ByReason extends Strategy implements IStrategy {

	public Exportable checkSection(Element page) {
		String pattern = ">([A-Za-zá-ú\\-\\s]+)</span> ([0-9.]+)";
		List<String> listMatches = doRegexp(page.toString(), pattern);
		return createCategory("Reason", listMatches);
	}

	public Element findSection(Document page) {
		return page.select("#main").first().child(0).child(11).child(1);
	}

	public void processListItem(Matcher matcher, List<String> listMatches) {
		listMatches.add(matcher.group(1).trim());
		listMatches.add(matcher.group(2).replace(".", "").trim());
	}

}
