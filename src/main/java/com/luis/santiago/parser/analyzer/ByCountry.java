package com.luis.santiago.parser.analyzer;

import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;
import com.luis.santiago.parser.report.ReportCategory;
import com.luis.santiago.parser.report.ReportEntry;

public class ByCountry extends Strategy implements IStrategy {

	public Exportable checkSection(Element page) {

		// Retrieve special "Spanish" pilgrims
		String spanish = checkSpanish(page);
		List<String> listMatches = checkNationalities(page);
		listMatches.add(0, "España");
		listMatches.add(1, spanish);

		return createCategory("Country", listMatches);

	}

	public Element findSection(Document page) {
		return page.select("#main").first().child(0).child(16);
	}

	public String checkSpanish(Element page) {
		List<String> listMatches = doRegexp(page.toString(), "Españoles:</span> ([.0-9]+) ");
		return listMatches.get(0);
	}

	public List<String> checkNationalities(Element page) {
		String pattern = "((([0-9]+) )|(([A-Z][a-zá-ú]+)( ([A-Z][a-zá-ú]+))*))";
		List<String> listMatches = doRegexp(page.child(1).toString(), pattern);
		listMatches = listMatches.subList(2, listMatches.size());
		return listMatches;
	}

	public void processListItem(Matcher matcher, List<String> listMatches) {
		listMatches.add(matcher.group(1).replace(".", "").trim());
	}

}
