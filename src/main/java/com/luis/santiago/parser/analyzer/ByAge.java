package com.luis.santiago.parser.analyzer;

import java.util.List;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.luis.santiago.parser.report.Exportable;

public class ByAge extends Strategy implements IStrategy {

	public Exportable checkSection(Element page) {

		List<String> listMatches = doRegexp(page.toString(), "son ([.0-9]+) ");

		listMatches.add(0, "<30");
		listMatches.add(2, "30-60");
		listMatches.add(4, ">60");

		return createCategory("Age", listMatches);
	}

	public Element findSection(Document page) {
		return page.select("#main").first().child(0).child(11).child(0);
	}

	public void processListItem(Matcher matcher, List<String> listMatches) {
		listMatches.add(matcher.group(1).replace(".", "").trim());
	}

}
