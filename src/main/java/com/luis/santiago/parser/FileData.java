package com.luis.santiago.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class FileData {
	public String fileName;
	public int[] startDate;
	public int[] endDate;
	public String outputName;

	private boolean verbose;

	public FileData() {

	}

	public FileData(String path) throws ParserConfigurationException, SAXException, IOException {
		this(path, false);
	}

	public FileData(String path, boolean verbose) throws ParserConfigurationException, SAXException, IOException {
		this.verbose = verbose;

		File file = new File(path);
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(file);

		fileName = doc.getElementsByTagName("url").item(0).getTextContent();
		if (verbose) {
			System.out.println("Setting fileName as " + fileName);
		}
		outputName = doc.getElementsByTagName("output").item(0).getTextContent();
		if (verbose) {
			System.out.println("Setting outputName as " + outputName);
		}
		startDate = new int[] {
				Integer.parseInt(doc.getElementsByTagName("start").item(0).getChildNodes().item(0).getTextContent()),
				Integer.parseInt(doc.getElementsByTagName("start").item(0).getChildNodes().item(1).getTextContent()) };

		if (verbose) {
			System.out.println("Setting startDate as " + startDate[0] + "/" + startDate[1]);
		}

		endDate = new int[] {
				Integer.parseInt(doc.getElementsByTagName("end").item(0).getChildNodes().item(0).getTextContent()),
				Integer.parseInt(doc.getElementsByTagName("end").item(0).getChildNodes().item(1).getTextContent()) };

		if (verbose) {
			System.out.println("Setting endDate as " + endDate[0] + "/" + endDate[1]);
		}
	}

	ArrayList<int[]> combineDates() {
		ArrayList<int[]> dates = new ArrayList<int[]>();
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.set(startDate[1], startDate[0], 1);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.set(endDate[1], endDate[0], 1);
		endCalendar.add(Calendar.MONTH, 1);

		while (startCalendar.get(Calendar.MONTH) != endCalendar.get(Calendar.MONTH)
				|| startCalendar.get(Calendar.YEAR) != endCalendar.get(Calendar.YEAR)) {
			dates.add(new int[] { startCalendar.get(Calendar.MONTH), startCalendar.get(Calendar.YEAR) });
			startCalendar.add(Calendar.MONTH, 1);
		}
		return dates;
	}
}
