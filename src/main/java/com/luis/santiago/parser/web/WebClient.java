package com.luis.santiago.parser.web;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import com.luis.santiago.parser.web.Url;

public class WebClient {

	static final WebClient instance = new WebClient();

	public static WebClient getInstance() {
		return instance;
	}

	private WebClient() {
	};

	public Document getUrl(String url) throws IOException {
		return Jsoup.connect(url).get();
	}

	public static Url getUrlBuilder() {
		return new Url();
	}

}
