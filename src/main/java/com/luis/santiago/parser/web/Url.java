package com.luis.santiago.parser.web;

import java.util.ArrayList;

import com.google.common.base.Joiner;

public class Url {
	public ArrayList<String> queryParams;
	public String url;

	public Url() {
		this.url = "";
		this.queryParams = new ArrayList<String>();
	}

	public Url(String url) {
		this();
		if (!url.isEmpty()) {
			this.url = url;
		}

	}

	public Url setUrl(String url) {
		this.url = url;
		return this;
	}

	public Url addQueryParam(String query, String param) {
		this.queryParams.add(query + "=" + param);
		return this;
	}

	public Url clearQueryParams() {
		this.queryParams.clear();
		return this;
	}

	public String build() {
		String jointQueryString = Joiner.on('&').join(this.queryParams);
		return this.url + ((jointQueryString.length() > 0) ? "?" + jointQueryString : "");
	}
}
