package com.luis.santiago.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.luis.santiago.parser.analyzer.*;

import com.luis.santiago.parser.web.*;

/**
 * Creates a series of XML files from
 * http://peregrinossantiago.es/esp/oficina-del-peregrino/estadisticas/ First
 * parameter of main defines configuration file. Sample at
 * "/src/resources/config/configuration.xml"
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		String url = args[0];
		FileData fileData = new FileData(url, true);
		ArrayList<int[]> dates = fileData.combineDates();
		WebClient client = WebClient.getInstance();
		Url builder = WebClient.getUrlBuilder();
		builder.setUrl(fileData.fileName);

		Analyzer analyzer = new Analyzer();
		analyzer.addStrategy(new ByAge()).addStrategy(new ByCountry()).addStrategy(new ByGender())
				.addStrategy(new ByJob()).addStrategy(new ByMethod()).addStrategy(new ByReason())
				.addStrategy(new ByStartPlace()).addStrategy(new ByWay());

		int year, month;

		String query;
		Document partialXml;

		for (int[] date : dates) {

			year = date[1];
			month = date[0] + 1;

			query = builder.clearQueryParams().addQueryParam("anio", Integer.toString(year))
					.addQueryParam("mes", Integer.toString(month)).build();

			analyzer.setPage(client.getUrl(query));
			partialXml = analyzer.analyze("Basic report for " + year + "/" + month, month, year);

			save(partialXml, fileData.outputName + year + "_" + month + ".xml");

		}

		System.out.println("Task finished");
	}

	static void save(Document xml, String path) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(xml);
		StreamResult result = new StreamResult(new File(path));

		transformer.transform(source, result);
		System.out.println("File at " + path + " has been created");
	}

}
