package com.luis.santiago.parser.report;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ReportEntry extends Report implements Exportable {

	private String value;

	public ReportEntry(String key, String value) {
		this.name = key;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public boolean equals(Object other) {

		if (other == this) {
			return true;
		}
		if (other == null || !(other instanceof ReportEntry)) {
			return false;
		}

		ReportEntry otherReport = (ReportEntry) other;
		return (otherReport.getName().equals(this.getName()) && otherReport.value == this.value);
	}

	public Element toXml(Document document) throws Exception {
		Element item = null;
		item = document.createElement(this.getName());
		item.appendChild(document.createTextNode(this.getValue()));

		return item;
	}

}
