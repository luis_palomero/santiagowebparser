package com.luis.santiago.parser.report;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class Report implements Exportable {

	public static String nodeToString(Node node) throws TransformerException {
		StringWriter buf = new StringWriter();
		Transformer xform = TransformerFactory.newInstance().newTransformer();
		xform.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		xform.transform(new DOMSource(node), new StreamResult(buf));
		return (buf.toString());
	}

	protected String name;

	public Report setName(String categoryName) {
		this.name = categoryName;
		return this;
	}

	public String getName() {
		return name;
	}

	public Element toXml() throws Exception {

		Document document;

		document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		return this.toXml(document);
	}

	abstract public Element toXml(Document document) throws Exception;
}
