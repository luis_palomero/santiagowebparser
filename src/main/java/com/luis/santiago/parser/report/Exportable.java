package com.luis.santiago.parser.report;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public interface Exportable {
	public Element toXml() throws Exception;

	public Node toXml(Document document) throws Exception;
}
