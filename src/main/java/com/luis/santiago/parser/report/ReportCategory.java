package com.luis.santiago.parser.report;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ReportCategory extends Report {

	protected ArrayList<Exportable> items;

	public ReportCategory(String name) {
		this.setName(name);
		items = new ArrayList<Exportable>();
	}

	public ArrayList<Exportable> getItems() {
		return items;
	}

	public Report addItem(Exportable item) {
		items.add(item);
		return this;
	}

	public Element toXml(Document document) throws Exception {

		Element node = null;

		node = document.createElement(this.getName());
		for (Exportable item : this.getItems()) {
			node.appendChild(item.toXml(document));
		}

		return node;
	}

}
