package com.luis.santiago.parser.report;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import com.luis.santiago.parser.report.ReportEntry;

public class ReportEntryTest {

	private ReportEntry entry;

	@Before
	public void initialize() {
		entry = new ReportEntry("test", "42");
	}

	@Test
	public void testReportEntry() {
		assertTrue(entry instanceof ReportEntry);
	}

	@Test
	public void testEqualsNull() {
		assertFalse(entry.equals(null));
	}

	@Test
	public void testEqualsValid() {
		ReportEntry otherEntry = new ReportEntry("test", "42");
		assertTrue(entry.equals(otherEntry));
	}

	@Test
	public void testEqualsDifferentName() {
		ReportEntry otherEntry = new ReportEntry("test2", "42");
		assertFalse(entry.equals(otherEntry));
	}

	@Test
	public void testEqualsDifferentValue() {
		ReportEntry otherEntry = new ReportEntry("test", "43");
		assertFalse(entry.equals(otherEntry));
	}

	@Test
	public void testGetName() {
		assertEquals("test", entry.getName());
	}

	@Test
	public void testGetNumber() {
		assertEquals("42", entry.getValue());
	}

	@Test
	public void testToXml() throws Exception {
		Element node = entry.toXml();
		assertEquals("test", node.getNodeName());
		assertEquals("42", node.getFirstChild().getNodeValue());
	}
}
