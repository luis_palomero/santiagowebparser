package com.luis.santiago.parser.report;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.luis.santiago.parser.report.ReportCategory;
import com.luis.santiago.parser.report.ReportEntry;

public class ReportCategoryTest {

	private ReportCategory category = null;

	@Before
	public void initialize() {
		category = new ReportCategory("testReport");
	}

	@Test
	public void testToXmlWithoutItems() throws Exception {
		Element node = category.toXml();
		assertEquals("testReport", node.getTagName());
		assertEquals(0, node.getChildNodes().getLength());
	}

	@Test
	public void testToXmlWithOneItem() throws Exception {
		category.addItem(new ReportEntry("hombres", "100"));
		Element node = category.toXml();
		assertEquals("testReport", node.getTagName());
		assertEquals(1, node.getChildNodes().getLength());
		assertEquals("hombres", node.getFirstChild().getNodeName());
		assertEquals("100", node.getFirstChild().getFirstChild().getNodeValue());
	}

	@Test
	public void testToXmlWithTwoItems() throws Exception {
		category.addItem(new ReportEntry("hombres", "100"));
		category.addItem(new ReportEntry("mujeres", "101"));
		Element node = category.toXml();
		assertEquals("testReport", node.getTagName());
		assertEquals(2, node.getChildNodes().getLength());
		NodeList children = node.getChildNodes();

		assertEquals("hombres", children.item(0).getNodeName());
		assertEquals("100", children.item(0).getFirstChild().getNodeValue());
		assertEquals("mujeres", children.item(1).getNodeName());
		assertEquals("101", children.item(1).getFirstChild().getNodeValue());

	}

}
