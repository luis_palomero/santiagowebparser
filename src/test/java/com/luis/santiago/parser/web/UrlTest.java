package com.luis.santiago.parser.web;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import com.luis.santiago.parser.web.Url;

public class UrlTest {

	Url urlForTest;

	@Before
	public void initialize() {
		urlForTest = new Url();
	}

	@Test
	public void testUrlReturnsEmptyUrlWhenCalledByDefault() {
		assertEquals("Shouls use empty string as default", "", urlForTest.url);
		assertEquals("Should use empty queryparams list as default", 0, urlForTest.queryParams.size());
	}

	@Test
	public void testUrlReturnsNonEmptyUrlWhenCalledWithOne() {
		Url urlForTest = new Url("http://www.google.es");
		assertTrue("Should use parametrized string in builder", "http://www.google.es".equals(urlForTest.url));
		assertEquals("Should use empty queryparams list as default", 0, urlForTest.queryParams.size());
	}

	@Test
	public void testAddQueryParamAddingMultiple() {
		urlForTest.addQueryParam("q", "1");
		urlForTest.addQueryParam("qq", "2");
		assertEquals("After two insertions we must have two items", urlForTest.queryParams.size(), 2);
	}

	@Test
	public void testBuildWithoutQueryParams() {
		assertEquals("Built url without params should be valid, without '?'", "http://a.com",
				urlForTest.setUrl("http://a.com").build());
	}

	@Test
	public void testBuildWithQueryParams() {
		urlForTest.setUrl("http://a.com").addQueryParam("q", "1").addQueryParam("q2", "2");

		assertEquals("Built url should be valid", "http://a.com?q=1&q2=2", urlForTest.build());
	}

}
