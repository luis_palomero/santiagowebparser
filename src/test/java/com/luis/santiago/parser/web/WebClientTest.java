package com.luis.santiago.parser.web;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import static org.mockito.Mockito.*;
import org.mockito.Mockito;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import com.luis.santiago.parser.web.WebClient;

public class WebClientTest {

	@Test
	public void testGetUrlBuilder() {
		Url urlBuilder = WebClient.getUrlBuilder();
		assertTrue(urlBuilder instanceof Url);
	}

	@Test(expected = IOException.class)
	public void testGetWithError() throws IOException {
		WebClient client = Mockito.mock(WebClient.class);
		String url = new String("http://invalidurl.alalalal");
		doThrow(new IOException()).when(client).getUrl(url);
		client.getUrl(url);
	}

	@Test(expected = org.jsoup.HttpStatusException.class)
	public void testGetUnknownUrl() throws IOException {
		WebClient client = WebClient.getInstance();
		client.getUrl("http://google.com/aaaaaaa");
	}

	/**
	 * This tests checks the mocking result of the file
	 * 
	 * @throws IOException
	 */
	@Test
	public void testGetValidUrl() throws IOException {

		WebClient client = Mockito.mock(WebClient.class);

		String localFileAddress = new String(
				System.getProperty("user.dir") + "/src/test/resources/parser/web/WebClientTest/index.html");
		File localFile = new File(localFileAddress);

		Document doc = Jsoup.parse(localFile, "UTF-8", "");
		when(client.getUrl("local")).thenReturn(doc);

		assertTrue(doc.toString().contains("Estadísticas de Peregrinos"));
	}
}
