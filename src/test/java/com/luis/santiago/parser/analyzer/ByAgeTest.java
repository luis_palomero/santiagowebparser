package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import java.io.File;
import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByAgeTest extends CategoryTests {

	@Before
	public void setUp() throws Exception {
		strategy = new ByAge();
		super.setUp();
	}

	@Test
	public void testFindSection() {
		Element section = strategy.findSection(page);
		assertTrue(section.toString().contains(
				"Menores de 30 años son 581 peregrinos (28,18%); de entre 30 y 60 son 1.318 (63,92%) y mayores de 60 años son 163 peregrinos (7,90%)."));
	}

	@Test
	public void testCheckSection() {
		Element section = strategy.findSection(page);
		ReportCategory byAge = (ReportCategory) strategy.checkSection(section);
		String xmlStr;

		assertEquals(3, byAge.getItems().size());

		try {
			xmlStr = Report.nodeToString(byAge.toXml());
			assertTrue(xmlStr.contains(
					"<Age><item><key>&lt;30</key><value>581</value></item><item><key>30-60</key><value>1318</value></item><item><key>&gt;60</key><value>163</value></item></Age>"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
