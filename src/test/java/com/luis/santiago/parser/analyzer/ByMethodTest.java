package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByMethodTest extends CategoryTests {

	Element section;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		strategy = new ByMethod();
		section = strategy.findSection(page);
	}

	@Test
	public void testCheckSection() {
		assertTrue(section.toString().contains("A pie"));
		assertTrue(section.toString().contains("bicicleta"));
		assertTrue(section.toString().contains("caballo"));
		assertTrue(section.toString().contains("silla de ruedas"));
	}

	@Test
	public void testFindSection() {
		ReportCategory byJob = (ReportCategory) strategy.checkSection(section);
		String xmlStr;

		assertEquals(4, byJob.getItems().size());

		try {
			xmlStr = Report.nodeToString(byJob.toXml());
			assertTrue(xmlStr.contains(
					"<Method><item><key>A pie</key><value>1911</value></item><item><key>Bicicleta</key><value>149</value></item><item><key>Caballo</key><value>2</value></item><item><key>Silla de ruedas</key><value>0</value></item></Method>"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
