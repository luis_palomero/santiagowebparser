package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByGenderTest extends CategoryTests {

	Element section;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		strategy = new ByGender();
		section = strategy.findSection(page);

	}

	@Test
	public void testCheckSection() {
		assertTrue(section.toString().contains("son mujeres"));
		assertTrue(section.toString().contains("hombres"));
	}

	@Test
	public void testFindSection() {
		ReportCategory byGender = (ReportCategory) strategy.checkSection(section);
		String xmlStr;
		assertEquals(2, byGender.getItems().size());

		try {
			xmlStr = Report.nodeToString(byGender.toXml());
			assertTrue(xmlStr.contains(
					"<Gender><item><key>Mujeres</key><value>694</value></item><item><key>Hombres</key><value>1368</value></item></Gender>"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
