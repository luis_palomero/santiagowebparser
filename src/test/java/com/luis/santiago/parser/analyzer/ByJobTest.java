package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByJobTest extends CategoryTests {

	Element section;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		strategy = new ByJob();
		section = strategy.findSection(page);
	}

	@Test
	public void testCheckSection() {
		ReportCategory byJob = (ReportCategory) strategy.checkSection(section);
		String xmlStr;

		assertEquals(17, byJob.getItems().size());

		try {
			xmlStr = Report.nodeToString(byJob.toXml());
			assertTrue(xmlStr.contains(
					"<Job><item><key>Empleados</key><value>547</value></item><item><key>Liberales</key><value>379</value>"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testFindSection() {
		assertTrue(section.toString().contains("Liberales"));
		assertTrue(section.toString().contains("Empleados"));
	}

}
