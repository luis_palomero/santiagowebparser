package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByReasonTest extends CategoryTests {

	Element section;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		strategy = new ByReason();
		section = strategy.findSection(page);

	}

	@Test
	public void testCheckSection() {
		assertTrue(section.toString().contains("Religiosa"));
		assertTrue(section.toString().contains("Sólo cultural"));
	}

	@Test
	public void testFindSection() {
		ReportCategory byReason = (ReportCategory) strategy.checkSection(section);

		String xmlStr;
		assertEquals(3, byReason.getItems().size());

		try {
			xmlStr = Report.nodeToString(byReason.toXml());

			assertTrue(xmlStr.contains(
					"<Reason><item><key>Religiosa</key><value>585</value></item><item><key>Religiosa-cultural</key><value>1353</value></item><item><key>Sólo cultural</key><value>124</value></item></Reason>"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
