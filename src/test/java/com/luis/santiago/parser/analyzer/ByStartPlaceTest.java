package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByStartPlaceTest extends CategoryTests {

	Element section;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		strategy = new ByStartPlace();
		section = strategy.findSection(page);

	}

	@Test
	public void testCheckSection() {
		assertTrue(section.toString().contains("Sarria"));
		assertTrue(section.toString().contains("S. Jean P. Port"));
	}

	@Test
	public void testFindSection() {
		ReportCategory byReason = (ReportCategory) strategy.checkSection(section);

		String xmlStr;
		assertEquals(104, byReason.getItems().size());

		try {
			xmlStr = Report.nodeToString(byReason.toXml());
			assertTrue(xmlStr.contains("<Start><item><key>Sarria</key><value>472</value></item>"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
