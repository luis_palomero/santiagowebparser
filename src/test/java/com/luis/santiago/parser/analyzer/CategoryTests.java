package com.luis.santiago.parser.analyzer;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.junit.Before;

public abstract class CategoryTests {
	Document page;
	Strategy strategy;

	@Before
	public void setUp() throws Exception {

		String localFileAddress = new String(
				System.getProperty("user.dir") + "/src/test/resources/parser/web/WebClientTest/index.html");

		File input = new File(localFileAddress);

		page = Jsoup.parse(input, "UTF-8", "");

	}

}
