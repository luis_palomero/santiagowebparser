package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByWayTest extends CategoryTests {

	Element section;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		strategy = new ByWay();
		section = strategy.findSection(page);
	}

	@Test
	public void testCheckSection() {
		assertTrue(section.toString().contains("Frances-Camino de"));
		assertTrue(section.toString().contains("Otros caminos"));
	}

	@Test
	public void testFindSection() {
		ReportCategory byReason = (ReportCategory) strategy.checkSection(section);

		String xmlStr;
		assertEquals(9, byReason.getItems().size());

		try {
			xmlStr = Report.nodeToString(byReason.toXml());
			assertTrue(xmlStr.contains(
					"<Way><item><key>Frances-Camino de</key><value>1328</value></item><item><key>Portugues-Camino</key><value>322"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
