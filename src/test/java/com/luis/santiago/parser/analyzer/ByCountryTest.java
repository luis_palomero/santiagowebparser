package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import java.util.List;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import com.luis.santiago.parser.report.Report;
import com.luis.santiago.parser.report.ReportCategory;

public class ByCountryTest extends CategoryTests {

	Element section;

	@Before
	public void setUp() throws Exception {
		super.setUp();

		strategy = new ByCountry();
		section = strategy.findSection(page);

	}

	@Test
	public void testCheckSpanish() {
		ByCountry byCountry = (ByCountry) strategy;
		assertEquals("1096", byCountry.checkSpanish(section));
	}

	@Test
	public void testCheckNationalities() {
		List<String> nats = ((ByCountry) strategy).checkNationalities(section);
		assertEquals("Corea", nats.get(0));
		assertEquals("117", nats.get(1));
		assertEquals(16, nats.size());

	}

	@Test
	public void testFindSection() {
		assertTrue(section.toString().contains("Nacionalidades de los peregrinos"));
		assertTrue(section.toString().contains("Españoles"));
		assertTrue(section.toString().contains("Extranjeros"));
	}

	@Test
	public void testCheckSectionCountries() {
		ReportCategory byAge = (ReportCategory) strategy.checkSection(section);
		String xmlStr;

		assertEquals(9, byAge.getItems().size());

		try {
			xmlStr = Report.nodeToString(byAge.toXml());
			assertTrue(xmlStr.contains("<Country><item><key>España</key><value>1096</value>"));
			assertTrue(xmlStr.contains("<item><key>Corea</key><value>117</value>"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
