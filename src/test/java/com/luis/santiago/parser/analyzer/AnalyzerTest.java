package com.luis.santiago.parser.analyzer;

import static org.junit.Assert.*;

import java.io.File;

import com.luis.santiago.parser.analyzer.*;
import com.luis.santiago.parser.report.Report;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.NodeList;

public class AnalyzerTest {

	private Analyzer analyzer;

	@Before
	public void setUp() throws Exception {

		String localFileAddress = new String(
				System.getProperty("user.dir") + "/src/test/resources/parser/web/WebClientTest/index.html");

		File input = new File(localFileAddress);

		Document page = Jsoup.parse(input, "UTF-8", "");

		analyzer = new Analyzer().setPage(page);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddStrategies() {
		ByGender byGender = new ByGender();
		ByAge byAge = new ByAge();
		ByWay byWay = new ByWay();
		ByReason byReason = new ByReason();
		ByCountry byCountry = new ByCountry();
		ByJob byJob = new ByJob();
		ByStartPlace byStartPlace = new ByStartPlace();
		ByMethod byMethod = new ByMethod();
		analyzer.addStrategy(byGender).addStrategy(byAge).addStrategy(byWay).addStrategy(byReason)
				.addStrategy(byCountry).addStrategy(byJob).addStrategy(byStartPlace).addStrategy(byMethod);
		assertEquals(8, analyzer.sectionsToCheck.size());

	}

	@Test

	public void testAnalyzeTwoMethods() throws Exception {
		ByGender byGender = new ByGender();
		ByAge byAge = new ByAge();
		org.w3c.dom.Document xml;
		analyzer.addStrategy(byGender).addStrategy(byAge);

		xml = analyzer.analyze("Report for Jan 2015", 1, 2015);
		assertEquals(2, analyzer.sectionsToCheck.size());
		NodeList categories = xml.getElementsByTagName("Categories");
		assertEquals(
				"<Gender><item><key>Mujeres</key><value>694</value></item><item><key>Hombres</key><value>1368</value></item></Gender>",
				Report.nodeToString(categories.item(0).getFirstChild()));
		assertEquals(
				"<Age><item><key>&lt;30</key><value>581</value></item><item><key>30-60</key><value>1318</value></item><item><key>&gt;60</key><value>163</value></item></Age>",
				Report.nodeToString(categories.item(0).getFirstChild().getNextSibling()));

	}

}
