package com.luis.santiago.parser;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class FileDataTest {
	FileData fileData;

	@Before
	public void setUp() throws Exception {
		fileData = new FileData();
	}

	@Test
	public void testCombineDates() {
		fileData.startDate = new int[] { 0, 2004 };
		fileData.endDate = new int[] { 11, 2005 };

		ArrayList<int[]> combinedDates = fileData.combineDates();
		assertEquals(24, combinedDates.size());
		assertEquals(combinedDates.get(0)[0], 0);
		assertEquals(combinedDates.get(0)[1], 2004);
		assertEquals(combinedDates.get(1)[0], 1);
		assertEquals(combinedDates.get(1)[1], 2004);
		assertEquals(combinedDates.get(22)[0], 10);
		assertEquals(combinedDates.get(22)[1], 2005);
		assertEquals(combinedDates.get(23)[0], 11);
		assertEquals(combinedDates.get(23)[1], 2005);

	}

}
